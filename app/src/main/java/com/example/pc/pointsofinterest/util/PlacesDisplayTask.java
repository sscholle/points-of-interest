package com.example.pc.pointsofinterest.util;

/**
 * Created by DELL on 2016/05/07.
 */
import android.os.AsyncTask;
import android.util.Log;

import com.example.pc.pointsofinterest.util.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlacesDisplayTask extends AsyncTask<Object, Integer, List<HashMap<String, String>>> {

    JSONObject googlePlacesJson;
    GoogleMap googleMap;

    HashMap<String, String> mMarkerPlaceLink;// = new HashMap<String, String>();
    //ArrayList<Double> sLatLong;


    @Override
    protected List<HashMap<String, String>> doInBackground(Object... inputObj) {

        List<HashMap<String, String>> googlePlacesList = null;
        Places placeJsonParser = new Places();

        try {
            googleMap = (GoogleMap) inputObj[0];
            googlePlacesJson = new JSONObject((String) inputObj[1]);
            mMarkerPlaceLink = (HashMap<String, String>) inputObj[2];
            //sLatLong = (ArrayList<Double>)inputObj[3];

            Log.v("Places JSON: ",(String) inputObj[1] );
            googlePlacesList = placeJsonParser.parse(googlePlacesJson);
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        }
        return googlePlacesList;
    }

    @Override
    protected void onPostExecute(List<HashMap<String, String>> list) {
        googleMap.clear();
        if(list == null)
            return;

        for (int i = 0; i < list.size(); i++) {
            MarkerOptions markerOptions = new MarkerOptions();
            HashMap<String, String> googlePlace = list.get(i);
            double lat = Double.parseDouble(googlePlace.get("lat"));
            double lng = Double.parseDouble(googlePlace.get("lng"));
            String placeName = googlePlace.get("place_name");
            String vicinity = googlePlace.get("vicinity");
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);
            markerOptions.title(placeName + " : " + vicinity);
            Marker m = googleMap.addMarker(markerOptions);

            mMarkerPlaceLink.put(m.getId(), googlePlace.get("reference"));
        }
    }
}
