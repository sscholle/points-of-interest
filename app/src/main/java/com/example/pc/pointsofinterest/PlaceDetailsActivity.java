package com.example.pc.pointsofinterest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pc.pointsofinterest.util.PlaceDetailsJSONParser;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

public class PlaceDetailsActivity extends Activity {
    double sLatitude;
    double sLongitude;

    double dLatitude;
    double dLongitude;

    ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_details);

        // Getting place reference from the map
        String reference = getIntent().getStringExtra("reference");

        sLatitude = getIntent().getDoubleExtra("sLatitude", 0.0);
        sLongitude = getIntent().getDoubleExtra("sLongitude", 0.0);

        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");
        sb.append("reference=" + reference);
        sb.append("&sensor=true");
        sb.append("&key=" + GooglePlacesActivity.GOOGLE_API_KEY);

        imageView = (ImageView) findViewById(R.id.place_image);
        // Creating a new non-ui thread task to download Google place details
        PlacesTask placesTask = new PlacesTask();

        // Invokes the "doInBackground()" method of the class PlaceTask
        placesTask.execute(sb.toString());
    }

    ;

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        } catch (Exception e) {
            Log.d("Exception while getting", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * A class, to download Google Place Details
     */
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result) {
            ParserTask parserTask = new ParserTask();

            // Start parsing the Google place details in JSON format
            // Invokes the "doInBackground()" method of the class ParseTask
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Place Details in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, HashMap<String, String>> {

        JSONObject jObject;

        @Override
        protected HashMap<String, String> doInBackground(String... jsonData) {

            HashMap<String, String> hPlaceDetails = null;
            PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);
                // Start parsing Google place details in JSON format
                hPlaceDetails = placeDetailsJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return hPlaceDetails;
        }

        /**
         * Updates the details text views with the relevant data from the Retrieved places objects
         * Executed after the complete execution of doInBackground() method
         *
         * @param hPlaceDetails
         */
        @Override
        protected void onPostExecute(HashMap<String, String> hPlaceDetails) {
            ((TextView) findViewById(R.id.place_name)).setText(hPlaceDetails.get("name"));
            new DownloadImagesTask().execute(hPlaceDetails.get("icon"));
            ((TextView) findViewById(R.id.detail_vicinity)).setText(hPlaceDetails.get("vicinity"));
            ((TextView) findViewById(R.id.detail_location)).setText(hPlaceDetails.get("lat") + ", " + hPlaceDetails.get("lng"));
            ((TextView) findViewById(R.id.detail_address)).setText(hPlaceDetails.get("formatted_address"));
            ((TextView) findViewById(R.id.detail_phone)).setText(hPlaceDetails.get("formatted_phone"));
            ((TextView) findViewById(R.id.detail_website)).setText(hPlaceDetails.get("website"));
            ((TextView) findViewById(R.id.detail_rating)).setText(hPlaceDetails.get("rating"));
            ((TextView) findViewById(R.id.detail_intl_phone)).setText(hPlaceDetails.get("international_phone_number"));
            ((TextView) findViewById(R.id.detail_url)).setText(hPlaceDetails.get("url"));

            // get loction details for passing into the MapsActivity for directions
            String lat = hPlaceDetails.get("lat");
            String lng = hPlaceDetails.get("lng");
            dLatitude = Double.parseDouble(lat);
            dLongitude = Double.parseDouble(lng);
        }
    }

    /**
     * Get direction button is pressed, opening the Maps activity with Directions to the Place of interest
     *
     * @param v button that was pressed
     */
    public void getDirections(View v) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + sLatitude + "," + sLongitude + "&daddr=" + dLatitude + "," + dLongitude));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }

    /**
     * Helper to get the icon/image data for the place
     */
    public class DownloadImagesTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... urls) {
            return download_Image(urls[0]);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }

        private Bitmap download_Image(String url) {
            //---------------------------------------------------
            Bitmap bm = null;
            try {
                URL aURL = new URL(url);
                URLConnection conn = aURL.openConnection();
                conn.connect();
                InputStream is = conn.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                bm = BitmapFactory.decodeStream(bis);
                bis.close();
                is.close();
            } catch (IOException e) {

            }
            return bm;
            //---------------------------------------------------
        }
    }
}