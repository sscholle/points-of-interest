package com.example.pc.pointsofinterest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;


public class MainActivity extends ActionBarActivity {
    public static final String LOCATION_TYPE = "com.example.pc.pointsofinterest.LOCATION_TYPE";
    public static final String LOCATION_NAME = "com.example.pc.pointsofinterest.LOCATION_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * All button clicks are handled here
     * Launches the Maps activity with markers on the map regarding the selected type of place
     * @param view the button that was clicked
     */
    public void onButtonClicked(View view) {
        Intent mapIntent = new Intent(this, GooglePlacesActivity.class);
        switch (view.getId()) {
            case R.id.action_shopping:
                mapIntent.putExtra(LOCATION_TYPE, "grocery_or_supermarket"); // type
                mapIntent.putExtra(LOCATION_NAME, getResources().getString(R.string.shopping)); // name
                break;

            case R.id.action_service_station:
                mapIntent.putExtra(LOCATION_TYPE, "gas_station"); // type
                mapIntent.putExtra(LOCATION_NAME, getResources().getString(R.string.service_station)); // name
                break;

            case R.id.action_doctors:
                mapIntent.putExtra(LOCATION_TYPE, "doctor"); // type
                mapIntent.putExtra(LOCATION_NAME, getResources().getString(R.string.doctors)); // name
                break;

            case R.id.action_police:
                mapIntent.putExtra(LOCATION_TYPE, "police"); // type
                mapIntent.putExtra(LOCATION_NAME, getResources().getString(R.string.police)); // name
                break;

            case R.id.action_hotel:
                mapIntent.putExtra(LOCATION_TYPE, "hotel"); // type
                mapIntent.putExtra(LOCATION_NAME, getResources().getString(R.string.hotel)); // name
                break;
        }
        startActivity(mapIntent);
    }
}
